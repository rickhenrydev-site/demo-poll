const AWS = require('aws-sdk')
const axios = require('axios')

const dynamodb = new AWS.DynamoDB({
    apiVersion: '2012-08-10',
    region: 'us-east-1',
    accessKeyId: process.env.DYNAMO_ACCESS_KEY_ID,
    secretAccessKey: process.env.DYNAMO_SECRET_ACCESS_KEY,
})

function countCandidate(name, dynamo_table_name) {
    const params = {
        TableName: dynamo_table_name,
        ScanFilter: {
            Choice: {
                ComparisonOperator: 'EQ',
                AttributeValueList: [
                    {
                        S: name,
                    },
                ],
            },
        },
    }

    return dynamodb.scan(params).promise()
}

async function countCandidates(dynamo_table_name, candidates) {
    const results = {
        candidates: {},
        totalVotes: 0,
    }

    for (candidate of candidates) {
        const data = await countCandidate(candidate.name, dynamo_table_name)
        const votes = data.Count
        results.candidates[candidate.name] = votes
        results.totalVotes += votes
    }
    return results
}

exports.handler = async function(event, context, callback) {
    try {
        const { config } = event.queryStringParameters
        const configBucket = 'prod-dtl-poll-configs'

        const res = await axios.get(
            `https://${configBucket}.s3.amazonaws.com/config-${config}.json`
        )
        const { dynamo_table_name, candidates } = res.data

        const results = await countCandidates(dynamo_table_name, candidates)
        return callback(null, {
            statusCode: 200,
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': 'Content-Type',
            },
            body: JSON.stringify(results),
        })
    } catch (err) {
        console.error(err)
        return callback(null, {
            statusCode: 404,
            body: 'Could not get standings data',
        })
    }
}
