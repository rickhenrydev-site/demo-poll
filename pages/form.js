document.addEventListener('DOMContentLoaded', () => {
    const winnerDiv = document.getElementById('candidates')
    const urlParams = new URLSearchParams(window.location.search)
    const configNumber = urlParams.get('config')
    const configBucket =
        process.env.STAGE === 'prod' ? 'prod-dtl-poll-configs' : 'dev-dtl-poll-configs'
    const form = document.getElementById('contest-form')
    form.action = `/success?config=${configNumber}`
    fetch(`https://${configBucket}.s3.amazonaws.com/config-${configNumber}.json`, {
        mode: 'cors',
        headers: new Headers({'Content-Type': 'application/json'}),
    })
        .then(res => res.text())
        .catch(err => {
            document
                .getElementById('error')
                .appendChild(document.createTextNode(`Something has gone wrong: ${err}`))
        })
        .then(candidateData => {
            const {candidates, airtable_table_name, dynamo_table_name} = JSON.parse(
                candidateData
            )
            const airtableTableInput = document.getElementById('airtable_table_name')
            const dynamoTableInput = document.getElementById('dynamo_table_name')
            airtableTableInput.value = airtable_table_name
            dynamoTableInput.value = dynamo_table_name

            suffle(candidates)
            candidates.forEach((candidate, idx) => {
                /* html */
                winnerDiv.innerHTML += `
<br/>
    <div class="line">
            <label for="candidate-${idx}" id="candidate-${idx}-label" class="label-container">${candidate.name}
                <input type="radio" id="candidate-${idx}" name="winner" value="${candidate.name}">
                <span class="checkmark"></span>
            </label>
            <div class="arrow-container" data-index="${idx}">
                <span class="arrow-text" data-index="${idx}" id="arrow-text-${idx}">View More</span>
                <div class="downArrow" data-index="${idx}" id="arrow-${idx}"></div>
            </div>
        </div>
        <div id="bio-${idx}" style="display:none">
        <div class="bio-container">
        <div class="bio-text-photo">
                <a href="${candidate.profileURL}"  target="_blank" rel="noopener noreferrer" class="overlay-container">
                    <img src="${candidate.photoURL}" width="200px" height="200px" class="image"  />
                <div class="overlay">
                    <span class="overlay-text">
                    View Profile
                    </span>
                </div>
                </a>
            <div class="bio">
            <p style="margin-top: 0.5rem; padding-top: 0;">
                ${candidate.bio}
            </p>
            </div>
        </div>
        <div class="map-button-container">
                <a href="${candidate.profileURL}" class="button button-yellow view-link view-profile-button" target="_blank" rel="noopener noreferrer">View Profile</a>
                <a href="${candidate.mapURL}" class="button button-yellow view-link" target="_blank" rel="noopener noreferrer">View Map</a>
        </div>
        </div>
        `
            })
            const checkmarks = document.querySelectorAll('.label-container')
            checkmarks.forEach(checkmark => {
                checkmark.addEventListener('click', clearDisabled)
            })
            const arrowContainers = document.querySelectorAll('.arrow-container')
            arrowContainers.forEach(arrowContainer => {
                arrowContainer.addEventListener('click', e => {
                    const idx = e.target.dataset.index
                    const arrow = document.getElementById(`arrow-${idx}`)
                    const arrowText = document.getElementById(`arrow-text-${idx}`)
                    const bio = document.getElementById(`bio-${idx}`)
                    if (bio.style.display === 'none') {
                        bio.style.display = 'block'
                        arrow.classList.remove('downArrow')
                        arrow.classList.add('upArrow')
                        arrowText.textContent = 'View Less'
                    } else {
                        bio.style.display = 'none'
                        arrow.classList.remove('upArrow')
                        arrow.classList.add('downArrow')
                        arrowText.textContent = 'View More'
                    }
                })
            })
        })
})

function clearDisabled() {
    document.querySelector('button[type="submit"]').removeAttribute('disabled')
}

function suffle(arr) {
    let currentIndex = arr.length
    let temporaryValue, randomIndex

    while (0 !== currentIndex) {
        randomIndex = Math.floor(Math.random() * currentIndex)
        currentIndex -= 1
        temporaryValue = arr[currentIndex]
        arr[currentIndex] = arr[randomIndex]
        arr[randomIndex] = temporaryValue
    }
}
